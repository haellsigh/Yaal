'use strict';

/* Controllers */

var YaalControllers = angular.module('YaalControllers', ['ui.bootstrap']);

// MAIN CONTROLLER   //
// MAIN CONTROLLER   //

YaalControllers.controller('MainController', ['$scope',
    function($scope) {
    }
]);

// ANIME CONTROLLERS //
// ANIME CONTROLLERS //

YaalControllers.controller('AnimeCreateController', ['$scope', '$routeParams',
    function($scope, $routeParams) {
    }
]);

YaalControllers.controller('AnimeListController', ['$scope', '$routeParams',
    function($scope, $routeParams) {
    }
]);

YaalControllers.controller('AnimeDetailController', ['$scope', '$routeParams',
    function($scope, $routeParams) {
    }
]);

// USER CONTROLLERS  //
// USER CONTROLLERS  //

YaalControllers.controller('UserCreateController', ['$scope', '$routeParams',
    function($scope, $routeParams) {
    }
]);

YaalControllers.controller('UserListController', ['$scope', '$routeParams',
    function($scope, $routeParams) {
    }
]);

YaalControllers.controller('UserDetailController', ['$scope', '$routeParams',
    function($scope, $routeParams) {
    }
]);