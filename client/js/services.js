/**
* Created with Yaal.
* User: nelieru
* Date: 2014-04-14
* Time: 04:12 PM
*/

'use strict';

var YaalServices = angular.module('YaalServices', ['ngResource']);

YaalServices.factory('Users', ['$resource', 
    function($resource) {
        return $resource('http://dotish-saucer.codio.io:6112/user/:userName', {}, {
            query: {
                method: 'GET', 
                params: {userName: ''},
                isArray: false
            }
        });
    }]);

YaalServices.factory('Animes', ['$resource', 
    function($resource) {
        return $resource('http://dotish-saucer.codio.io:6112/anime/:animeId', {}, {
            query: {
                method: 'GET', 
                params: {animeId: ''}, 
                isArray: false
            }
        });
    }]);