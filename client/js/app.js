'use strict';

var Yaal = angular.module('Yaal', ['ngRoute', 'ngGrid', 'YaalControllers', 'YaalServices']);

Yaal.config(['$routeProvider', '$locationProvider', '$httpProvider', 
    function($routeProvider, $locationProvider, $httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        
        $routeProvider.
        when('/', {
            templateUrl: 'templates/main.html',
            controller: 'MainController'
        }).
        //////////////////////////////////////////////////
        when('/anime/add', {
            templateUrl: 'templates/anime/create.html',
            controller: 'AnimeCreateController'
        }).
        when('/anime/list', {
            templateUrl: 'templates/anime/list.html',
            controller: 'AnimeListController'
        }).
        when('/anime/:animeId', {
            templateUrl: 'templates/anime/detail.html',
            controller: 'AnimeDetailController'
        }).
        //////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        when('/user/add', {
            templateUrl: 'templates/user/create.html',
            controller: 'UserCreateController'
        }).
        when('/user/list', {
            templateUrl: 'templates/user/list.html',
            controller: 'UserListController'
        }).
        when('/user/:username', {
            templateUrl: 'templates/user/detail.html',
            controller: 'UserDetailController'
        }).
        //////////////////////////////////////////////////
        otherwise({
            redirectTo: '/'
        });
    }
]);