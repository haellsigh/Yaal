**Yaal**
========
----------
**Yet Another Anime List** open source project along with it's RESTful api.


----------

service     | port
:---------- | :---:
database    | 27017
api         | 6112
web-client  | 80


----------
## Starting the RESTful API ##

**1. Start the mongodb service**

```
$ parts start mongodb  
```

**2. Run the RESTful API**

```
$ python run.py
```

**3. Enjoy !**
```
http://{{domain}}
```

----------
## Starting RockMongoDB database management ##

**1. Start apache2 service**
```
$ parts start apache2
```

**2. Click 'Access RockMongoDB'**